package cn.ydxiaoshuai.modules.controller;

import cn.hutool.core.util.CharsetUtil;
import cn.ydxiaoshuai.common.api.vo.api.RedisInfoBean;
import cn.ydxiaoshuai.common.aspect.annotation.AutoLog;
import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import cn.ydxiaoshuai.common.util.RedisUtil;
import cn.ydxiaoshuai.modules.util.ApiBeanUtil;
import com.alibaba.fastjson.JSON;
import cn.ydxiaoshuai.common.util.oConvertUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URLDecoder;
import java.util.List;

/**
 * @author 小帅丶
 * @Description Redis-常量修改
 * @className RedisRestController
 * @Date 2020年9月21日16:43:18
 **/
@Scope("prototype")
@Slf4j
@Api(tags = "Redis-常量修改")
@RestController
@RequestMapping("/rest/redis")
public class RedisRestController extends ApiRestController {
    @Autowired
    private ApiBeanUtil apiBeanUtil;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * @param key_name key的名称
     * @param key_value 要更新的key的值
     * @return org.springframework.http.ResponseEntity<java.lang.Object>
     * @Description 修改一些变量
     * @Author 小帅丶
     * @Date 2019年10月23日
     **/
    @AutoLog(value = "Redis常量修改API")
    @ApiOperation(value = "Redis常量修改API", notes = "Redis常量修改API")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, name = "key_name", value = "key的名称"),
            @ApiImplicitParam(required = true, name = "key_value", value = "要更新的key的值")
    })
    @RequestMapping(value = {"/update/{key_name}/{key_value}","/update"}, method = {RequestMethod.GET})
    public ResponseEntity<Object> getRotatyList(@PathVariable(required = false) String key_name, @PathVariable(required = false) String key_value, HttpServletRequest request, HttpServletResponse response) {
        RedisInfoBean bean = new RedisInfoBean();
        try {
            if(oConvertUtils.isEmpty(key_name)||oConvertUtils.isEmpty(key_value)){
                bean.fail("not enough param","参数缺失,请检查", 410101);
            } else {
                boolean hasKey = redisUtil.hasKey(key_name);
                if(hasKey){
                    key_value = URLDecoder.decode(key_value, CharsetUtil.UTF_8);
                    boolean updateStatus = redisUtil.set(key_name, key_value);
                    RedisInfoBean.Data data = new RedisInfoBean.Data();
                    data.setKey_name(key_name);
                    data.setKey_value(key_value);
                    data.setUpdate_status(updateStatus);
                    bean.success("success","操作完成",data);
                } else {
                    bean.fail("key does not exist, please check","key不存在,请检查", 410103);
                }
            }
        } catch (Exception e) {
            bean.error("system error", "系统错误");
        }
        beanStr = JSON.toJSONString(bean);
        return new ResponseEntity<Object>(beanStr, httpHeaders, HttpStatus.OK);
    }

    /**
     * @param key_name key的名称
     * @param start 开始位置
     * @param end 结束位置
     * @return org.springframework.http.ResponseEntity<java.lang.Object>
     * @Description 修改一些变量
     * @Author 小帅丶
     * @Date 2019年10月23日
     **/
    @AutoLog(value = "RedisLKey查询API")
    @ApiOperation(value = "RedisLKey查询API", notes = "RedisLKey查询API")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, name = "key_name", value = "key的名称"),
            @ApiImplicitParam(required = true, name = "start", value = "开始位置"),
            @ApiImplicitParam(required = true, name = "end", value = "结束位置")
    })
    @RequestMapping(value = {"/get/{key_name}/{start}/{end}","/update"}, method = {RequestMethod.GET})
    public ResponseEntity<Object> getLKeyList(@PathVariable(required = false) String key_name,
                                                @PathVariable(required = false) long start,
                                                @PathVariable(required = false) long end,
                                                HttpServletRequest request, HttpServletResponse response) {
        RedisInfoBean bean = new RedisInfoBean();
        try {
            if(oConvertUtils.isEmpty(key_name)||oConvertUtils.isEmpty(start)||oConvertUtils.isEmpty(end)){
                bean.fail("not enough param","参数缺失,请检查", 410101);
            } else {
                boolean hasKey = redisUtil.hasKey(key_name);
                if(hasKey){
                    List<Object> objects = redisUtil.lGet(key_name, start, end);
                    bean.success("success","操作完成",objects);
                } else {
                    bean.fail("key does not exist, please check","key不存在,请检查", 410103);
                }
            }
        } catch (Exception e) {
            bean.error("system error", "系统错误");
        }
        beanStr = JSON.toJSONString(bean);
        return new ResponseEntity<Object>(beanStr, httpHeaders, HttpStatus.OK);
    }

}
