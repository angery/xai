package cn.ydxiaoshuai.modules.controller;

import cn.ydxiaoshuai.common.api.vo.api.ImgColorBean;
import cn.ydxiaoshuai.common.api.vo.api.ImgColorResponseBean;
import cn.ydxiaoshuai.common.api.vo.api.RecommendBean;
import cn.ydxiaoshuai.common.api.vo.api.RecommendResponseBean;
import cn.ydxiaoshuai.common.system.base.controller.ApiRestController;
import cn.ydxiaoshuai.common.util.api.JDAIUtil;
import cn.ydxiaoshuai.modules.conts.LogTypeConts;
import cn.ydxiaoshuai.modules.util.ApiBeanUtil;
import cn.ydxiaoshuai.modules.weixin.po.WXAccessToken;
import com.alibaba.fastjson.JSON;
import com.baidu.aip.util.Base64Util;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author 小帅丶
 * @className GarbageRestController
 * @Description 垃圾分类
 * @Date 2020/4/26-16:24
 **/
@Controller
@RequestMapping(value = "/rest/jdai")
@Scope("prototype")
@Slf4j
@Api(tags = "京东AI-API")
public class JDAIRestController extends ApiRestController {
    @Autowired
    private ApiBeanUtil apiBeanUtil;

    /**
     * @Description 穿衣搭配接口
     * @param file 图片文件
     * @return void
     * @Author 小帅丶
     * @Date 2020年11月2日
     **/
    @RequestMapping(value = "/clothing_match", method = {RequestMethod.POST}, produces="application/json;charset=UTF-8")
    public ResponseEntity<Object> clothingMatching(@RequestParam(value = "file") MultipartFile file) {
        log.info("方法路径{}", requestURI);
        RecommendBean bean = new RecommendBean();
        RecommendResponseBean recommendResponseBean = new RecommendResponseBean();
        try {
            startTime = System.currentTimeMillis();
            String imgBase64 = Base64Util.encode(file.getBytes());
            param = "image="+imgBase64+",version="+version+",outfit_num=10";
            recommendResponseBean = JDAIUtil.getRecommendBean(imgBase64);
            if(LogTypeConts.API_VERSION.equals(version)){
                WXAccessToken imgCheckBean = apiBeanUtil.checkImg(request, file);
                if(imgCheckBean.getErrcode()==0){
                    apiBeanUtil.dealClothingData(recommendResponseBean,bean);
                }else{
                    bean.fail("img fail", imgCheckBean.getErrmsg(),imgCheckBean.getErrcode());
                }
            }else{
                apiBeanUtil.dealClothingData(recommendResponseBean,bean);
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info("穿衣搭配接口出错了" + errorMsg);
            bean.error("system error", "系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("耗时{}",timeConsuming);
        beanStr = JSON.toJSONString(bean);
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip, param, requestURI, errorMsg, LogTypeConts.JDAI_CM,userId,userAgent);
        //响应的内容
        return new ResponseEntity<Object>(beanStr, httpHeaders, HttpStatus.OK);
    }

    /**
     * @Description 颜色识别接口
     * @param file 图片文件
     * @return void
     * @Author 小帅丶
     * @Date 2020年11月2日
     **/
    @RequestMapping(value = "/detect_color", method = {RequestMethod.POST}, produces="application/json;charset=UTF-8")
    public ResponseEntity<Object> detectColor(@RequestParam(value = "file") MultipartFile file) {
        log.info("方法路径{}", requestURI);
        ImgColorBean bean = new ImgColorBean();
        ImgColorResponseBean imgColorResponseBean = new ImgColorResponseBean();
        try {
            startTime = System.currentTimeMillis();
            String imgBase64 = Base64Util.encode(file.getBytes());
            param = "image="+imgBase64+",version="+version+",color_count=10";
            imgColorResponseBean = JDAIUtil.getColorBean(imgBase64);
            if(LogTypeConts.API_VERSION.equals(version)){
                WXAccessToken imgCheckBean = apiBeanUtil.checkImg(request, file);
                if(imgCheckBean.getErrcode()==0){
                    apiBeanUtil.dealColorData(imgColorResponseBean,bean);
                }else{
                    bean.fail("img fail", imgCheckBean.getErrmsg(),imgCheckBean.getErrcode());
                }
            }else{
                apiBeanUtil.dealColorData(imgColorResponseBean,bean);
            }
        } catch (Exception e) {
            errorMsg = e.getMessage();
            log.info("颜色识别查询接口出错了" + errorMsg);
            bean.error("system error", "系统错误");
        }
        //耗时
        timeConsuming = String.valueOf(System.currentTimeMillis() - startTime);
        log.info("耗时{}",timeConsuming);
        beanStr = JSON.toJSONString(bean);
        apiBeanUtil.putLog(bean.getLog_id(), timeConsuming, beanStr, ip, param, requestURI, errorMsg, LogTypeConts.JDAI_COLOR,userId,userAgent);
        //响应的内容
        return new ResponseEntity<Object>(beanStr, httpHeaders, HttpStatus.OK);
    }

}
