package cn.ydxiaoshuai.modules.conts;

/**
 * @author 小帅丶
 * @className LogTypeConts
 * @Description 常量类
 * @Date 2020/4/20-18:22
 **/
public class LogTypeConts {
    /**
     * API版本
     */
    public static final String API_VERSION = "2";
    /**
     * 首页轮播图
     */
    public static final Integer INDEX_SLIDE = 72001;
    /**
     * 首页导航
     */
    public static final Integer INDEX_NAV = 72002;
    /**
     * 首页滚动新闻
     */
    public static final Integer INDEX_NEWS = 72003;
    /**
     * 刷新文案
     */
    public static final Integer INDEX_COPYWRITE = 72004;
    /**
     * 微信登录
     */
    public static final Integer WX_LOGIN = 72012;
    /**
     * 微信授权更新
     */
    public static final Integer WX_OAUTH_DECRYPT= 72013;

    /**
     * 虚拟换妆DIY
     */
    public static final Integer FACE_TRANSFER_DIY= 72014;
    /**
     * 虚拟换妆
     */
    public static final Integer FACE_TRANSFER= 72015;
    /**
     * 人脸融合DIY
     */
    public static final Integer FACE_MERGE_DIY= 72016;
    /**
     * 人脸融合
     */
    public static final Integer FACE_MERGE= 72017;
    /**
     * 痘斑痣检测
     */
    public static final Integer FACE_ACNESPOTMOLE= 72018;
    /**
     * 皮肤光滑度
     */
    public static final Integer FACE_SKIN_SMOOTH= 72019;
    /**
     * 肤色检测
     */
    public static final Integer FACE_SKIN_COLOR= 72020;
    /**
     * 皱纹检测
     */
    public static final Integer FACE_WRINKLE= 72021;
    /**
     * 黑眼圈眼袋检测
     */
    public static final Integer FACE_EYES_ATTR= 72022;
    /**
     * 垃圾分类检测
     */
    public static final Integer GARBAGE_SEARCH= 72023;
    /**
     * 图片融合检测
     */
    public static final Integer IMAGE_COMBINATION= 72024;
    /**
     * 图片背景色替换
     */
    public static final Integer PHOTO_REPALCE_BGCOLOR= 72025;
    /**
     * 手相分析
     */
    public static final Integer PALM_DETECT= 72026;
    /**
     * 手相分析V2
     */
    public static final Integer PALM_DETECT_V2= 72027;
    /**
     * 图像违规检测
     */
    public static final Integer IMG_CHECK= 72028;
    /**
     * 中草药检测-EASYDL-DYI
     */
    public static final Integer EASYDL_DIY= 72029;
    /**
     * PP口罩检测
     */
    public static final Integer PP_MASK= 72030;
    /**
     * 手相分析
     */
    public static final Integer PALM_DETECT_PALM= 72031;
    /**
     * 人脸变老
     */
    public static final Integer LJ_GROWOLD= 72032;
    /**
     * 我的动物脸
     */
    public static final Integer LJ_ANIMALFACE= 72033;
    /**
     * 性别反转
     */
    public static final Integer LJ_GENDER_CONVERT= 72034;
    /**
     * 五官分析
     */
    public static final Integer FACE_ORGANS_DETECT= 72035;
    /**
     * 用户首次访问累计天数
     */
    public static final Integer USER_DAYS = 72036;
    /**
     * 图片转字符画
     */
    public static final Integer IMG_ASCII = 72037;

    /**
     * 图像识别
     */
    public static final Integer IMG_DETECT = 72038;
    /**
     * 图像处理
     */
    public static final Integer IMG_HANDLE = 72039;
    /**
     * 穿衣搭配
     */
    public static final Integer JDAI_CM= 72042;
    /**
     * 颜色识别
     */
    public static final Integer JDAI_COLOR= 72043;
    /**
     * 人脸驱动-创建
     */
    public static final Integer ADD_FACE_DRIVE = 72056;
    /**
     * 虚拟主播-创建
     */
    public static final Integer ADD_VIRTUAL_HUMAN = 72057;
}
