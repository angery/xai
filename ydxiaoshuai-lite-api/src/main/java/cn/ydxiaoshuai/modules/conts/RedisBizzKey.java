package cn.ydxiaoshuai.modules.conts;

/**
 * @author 小帅丶
 * @className RedisKey
 * @Description 一些业务的key
 * @Date 2020/9/21-16:03
 **/
public class RedisBizzKey {
    /**
     * 人脸驱动任务KEY
     **/
    public static String FACE_DRIVE_TASK = "FACE_DRIVE_TASK";
    /**
     * 虚拟主播任务KEY
     **/
    public static String VIRTUAL_HUMAN_TASK = "VIRTUAL_HUMAN_TASK";
    /**
     * 是否走接口检测图片
     **/
    public static String IS_CHECK_IMG = "IS_CHECK_IMG";
    /**
     * 是否走接口检测文本
     **/
    public static String IS_CHECK_MSG = "IS_CHECK_MSG";
    /**
     * 是否走接口检测图片-是
     **/
    public static String IS_CHECK_IMG_YES = "0";
    /**
     * 是否走接口检测图片-否
     **/
    public static String IS_CHECK_IMG_NO = "1";
    /**
     * 是否走接口检测文本-是
     **/
    public static String IS_CHECK_MSG_YES = "0";
    /**
     * 是否走接口检测文本-否
     **/
    public static String IS_CHECK_MSG_NO = "1";
}
