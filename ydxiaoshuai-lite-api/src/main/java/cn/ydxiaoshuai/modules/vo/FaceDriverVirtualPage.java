package cn.ydxiaoshuai.modules.vo;

import cn.ydxiaoshuai.common.api.vo.api.BaseBean;
import cn.ydxiaoshuai.modules.facedynamic.entity.FaceDynamicTask;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className FaceDriverVirtualResponseBean
 * @Description 响应参数
 * @Date 2021-05-12-14:18
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FaceDriverVirtualPage extends BaseBean {
    private IPage<FaceDynamicTask> data;
    public FaceDriverVirtualPage success(String msg, String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        return this;
    }
    public FaceDriverVirtualPage success(String msg, String msg_zh, IPage<FaceDynamicTask> data) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.data = data;
        return this;
    }

    public FaceDriverVirtualPage fail(String msg, String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public FaceDriverVirtualPage error(String msg, String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 500;
        return this;
    }
}
