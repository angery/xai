package cn.ydxiaoshuai.modules.faceeffects.service.impl;

import cn.ydxiaoshuai.modules.faceeffects.entity.FaceEffectsTransferTemplate;
import cn.ydxiaoshuai.modules.faceeffects.mapper.FaceEffectsTransferTemplateMapper;
import cn.ydxiaoshuai.modules.faceeffects.service.IFaceEffectsTransferTemplateService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 虚拟换妆模板图表
 * @Author: 小帅丶
 * @Date:   2020-09-04
 * @Version: V1.0
 */
@Service
public class FaceEffectsTransferTemplateServiceImpl extends ServiceImpl<FaceEffectsTransferTemplateMapper, FaceEffectsTransferTemplate> implements IFaceEffectsTransferTemplateService {

}
