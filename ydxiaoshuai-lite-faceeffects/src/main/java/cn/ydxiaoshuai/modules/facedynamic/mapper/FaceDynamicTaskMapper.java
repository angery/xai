package cn.ydxiaoshuai.modules.facedynamic.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import cn.ydxiaoshuai.modules.facedynamic.entity.FaceDynamicTask;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 人脸动态任务
 * @Author: 小帅丶
 * @Date:   2021-05-13
 * @Version: V1.0
 */
public interface FaceDynamicTaskMapper extends BaseMapper<FaceDynamicTask> {

}
