package cn.ydxiaoshuai.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.ydxiaoshuai.modules.system.entity.SysDataSource;

/**
 * @Description: 多数据源管理
 * @Author: 小帅丶
 * @Date: 2019-12-25
 * @Version: V1.0
 */
public interface SysDataSourceMapper extends BaseMapper<SysDataSource> {

}
