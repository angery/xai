package cn.ydxiaoshuai.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ydxiaoshuai.modules.system.entity.SysFillRule;
import cn.ydxiaoshuai.modules.system.mapper.SysFillRuleMapper;
import cn.ydxiaoshuai.modules.system.service.ISysFillRuleService;
import org.springframework.stereotype.Service;

/**
 * @Description: 填值规则
 * @Author: 小帅丶
 * @Date: 2019-11-07
 * @Version: V1.0
 */
@Service("sysFillRuleServiceImpl")
public class SysFillRuleServiceImpl extends ServiceImpl<SysFillRuleMapper, SysFillRule> implements ISysFillRuleService {

}
