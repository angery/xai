package cn.ydxiaoshuai.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.ydxiaoshuai.modules.system.entity.SysPosition;
import cn.ydxiaoshuai.modules.system.mapper.SysPositionMapper;
import cn.ydxiaoshuai.modules.system.service.ISysPositionService;
import org.springframework.stereotype.Service;

/**
 * @Description: 职务表
 * @Author: 小帅丶
 * @Date: 2019-09-19
 * @Version: V1.0
 */
@Service
public class SysPositionServiceImpl extends ServiceImpl<SysPositionMapper, SysPosition> implements ISysPositionService {

}
