package cn.ydxiaoshuai.common.api.vo.palm;

import lombok.Data;

/**
 * @Description 上传返回的对象
 * @author 小帅丶
 * @className PalmImagesResponseBean
 * @Date 2020/1/3-10:59
 **/
@Data
public class PalmUploadImgUrlRequestBean {

    /**
     * business : ZXCS_手相研究院
     * url : https://uploadzxcs.ggwan.com/image/dashuju-oss/4a91e416e7cbbe-444x444.jpg
     */

    private String business ="ZXCS_手相研究院";
    private String url;
}
