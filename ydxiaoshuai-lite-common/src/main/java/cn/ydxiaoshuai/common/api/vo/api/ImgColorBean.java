package cn.ydxiaoshuai.common.api.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className GarbageBean
 * @Description 颜色识别返回页面的对象
 * @Date 2020年11月2日
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImgColorBean extends BaseBean {
    /** 具体返回的内容 */
    private ImgColorResponseBean.ResultBean data;

    public ImgColorBean success(String msg, String msg_zh, ImgColorResponseBean.ResultBean data) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        this.data = data;
        return this;
    }
    public ImgColorBean fail(String msg, String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public ImgColorBean error(String msg, String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 500;
        return this;
    }
}
