package cn.ydxiaoshuai.common.api.vo.linghitai;

import lombok.Data;

/**
 * @Description 人脸变老返回的对象
 * @author 小帅丶
 * @className GrowOldResponseBean
 * @Date 2020/1/8-17:22
 **/
@Data
public class GrowOldResponseBean {

    /**
     * code : 0
     * msg : success
     * data : {"image_url":"https://bs-ai.ggwan.com/platform/prod/b95b9d63fc774cc5941c6d899bd817ce202001.jpg","id":"b95b9d63fc774cc5941c6d899bd817ce202001"}
     */

    private int code;
    private String msg;
    private DataBean data;

    @Data
    public static class DataBean {
        /**
         * image_url : https://bs-ai.ggwan.com/platform/prod/b95b9d63fc774cc5941c6d899bd817ce202001.jpg
         * id : b95b9d63fc774cc5941c6d899bd817ce202001
         */
        private String image_url;
        private String id;
        private String similarity;

    }
}
