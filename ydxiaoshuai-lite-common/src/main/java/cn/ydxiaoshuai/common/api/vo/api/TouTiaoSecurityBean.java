package cn.ydxiaoshuai.common.api.vo.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

/**
 * @author 小帅丶
 * @className TouTiaoSecurityBean
 * @Description 头条内容安全检查返回页面的对象
 * @Date 2020年6月17日
 **/
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TouTiaoSecurityBean extends BaseBean{

    public TouTiaoSecurityBean success(String msg, String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 200;
        return this;
    }
    public TouTiaoSecurityBean fail(String msg, String msg_zh, Integer code) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = code;
        return this;
    }
    public TouTiaoSecurityBean error(String msg, String msg_zh) {
        this.msg = msg;
        this.msg_zh = msg_zh;
        this.code = 500;
        return this;
    }
}
