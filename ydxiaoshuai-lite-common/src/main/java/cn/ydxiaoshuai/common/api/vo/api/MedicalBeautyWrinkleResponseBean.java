package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className MedicalBeautyWrinkleResponseBean
 * @Description 皱纹检测
 * @Date 2020/6/17-17:41
 **/
@NoArgsConstructor
@Data
public class MedicalBeautyWrinkleResponseBean {


    private long error_code;
    private String error_msg;
    private long log_id;
    private long timestamp;
    private long cached;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {


        private int face_num;
        private List<FaceListBean> face_list;

        @NoArgsConstructor
        @Data
        public static class FaceListBean {


            private String face_token;
            private LocationBean location;
            private WrinkleBean wrinkle;

            @NoArgsConstructor
            @Data
            public static class LocationBean {

                private double left;
                private double top;
                private int width;
                private int height;
                private int degree;
            }

            @NoArgsConstructor
            @Data
            public static class WrinkleBean {

                private int wrinkle_num;
                private List<List<WrinkleDataBean>> wrinkle_data;

                @NoArgsConstructor
                @Data
                public static class WrinkleDataBean {

                    private int x;
                    private int y;
                }
            }
        }
    }
}
