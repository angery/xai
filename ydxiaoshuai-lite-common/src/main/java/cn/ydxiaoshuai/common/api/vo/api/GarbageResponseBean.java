package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className GarbageResponseBean
 * @Description 接口返回的对象
 * @Date 2020/4/26-16:46
 **/
@NoArgsConstructor
@Data
public class GarbageResponseBean {

    private String code;
    private boolean charge;
    private int remain;
    private int remainTimes;
    private int remainSeconds;
    private String msg;
    private ResultBean result;

    @NoArgsConstructor
    @Data
    public static class ResultBean {

        private int status;
        private String message;
        private List<GarbageInfoBean> garbage_info;

        @NoArgsConstructor
        @Data
        public static class GarbageInfoBean {

            private String cate_name;
            private String city_id;
            private String city_name;
            private double confidence;
            private String garbage_name;
            private String ps;
        }
    }
}
