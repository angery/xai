package cn.ydxiaoshuai.common.api.vo.api;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author 小帅丶
 * @className FaceDetectResponse
 * @Description 人脸检测
 * @Date 2020/10/10-10:52
 **/
@NoArgsConstructor
@Data
public class FaceDetectResponse {

    private ResultBean result;
    private long log_id;
    private String error_msg;
    private int cached;
    private int error_code;
    private int timestamp;

    @NoArgsConstructor
    @Data
    public static class ResultBean {

        private int face_num;
        private List<FaceListBean> face_list;
        private String deal_image;//原图框选后的图片

        @NoArgsConstructor
        @Data
        public static class FaceListBean {

            private ExpressionBean expression;
            private FaceShapeBean face_shape;
            private double beauty;
            private GenderBean gender;
            private RaceBean race;
            private EyeStatusBean eye_status;
            private int face_probability;
            private double spoofing;
            private QualityBean quality;
            private GlassesBean glasses;
            private EmotionBean emotion;
            private FaceTypeBean face_type;
            private AngleBean angle;
            private String face_token;
            private LocationBean location;
            private int age;
            private MaskBean mask;
            private String cut_image;//单人脸

            @NoArgsConstructor
            @Data
            public static class ExpressionBean {

                private int probability;
                private String type;
            }

            @NoArgsConstructor
            @Data
            public static class FaceShapeBean {

                private double probability;
                private String type;
            }

            @NoArgsConstructor
            @Data
            public static class GenderBean {

                private double probability;
                private String type;
            }

            @NoArgsConstructor
            @Data
            public static class RaceBean {

                private double probability;
                private String type;
            }

            @NoArgsConstructor
            @Data
            public static class EyeStatusBean {

                private int right_eye;
                private double left_eye;
            }

            @NoArgsConstructor
            @Data
            public static class QualityBean {

                private int illumination;
                private OcclusionBean occlusion;
                private int blur;
                private int completeness;

                @NoArgsConstructor
                @Data
                public static class OcclusionBean {

                    private int right_eye;
                    private int nose;
                    private int mouth;
                    private int left_eye;
                    private int left_cheek;
                    private double chin_contour;
                    private double right_cheek;
                }
            }

            @NoArgsConstructor
            @Data
            public static class GlassesBean {

                private int probability;
                private String type;
            }

            @NoArgsConstructor
            @Data
            public static class EmotionBean {

                private double probability;
                private String type;
            }

            @NoArgsConstructor
            @Data
            public static class FaceTypeBean {

                private int probability;
                private String type;
            }

            @NoArgsConstructor
            @Data
            public static class AngleBean {

                private double roll;
                private double pitch;
                private double yaw;
            }

            @NoArgsConstructor
            @Data
            public static class LocationBean {

                private double top;
                private double left;
                private int rotation;
                private int width;
                private int height;
            }

            @NoArgsConstructor
            @Data
            public static class MaskBean {

                private int probability;
                private int type;
            }
        }
    }
}

