package cn.ydxiaoshuai.common.api.vo.faceorgans;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 小帅丶
 * @className FaceFortuneBean
 * @Description 今日运势
 * @Date 2020/9/21-14:35
 **/
@NoArgsConstructor
@Data
public class FaceFortuneBean {

    /**
     * extData : {"template_name":"atom/fortuneToday","template_id":"G694"}
     * commonData : {"feRoot":"https://mms-static.cdn.bcebos.com/graph/graphfe/static","os":"ios","sidList":"10007_10802_10915_10913_11006_10921_10905_10015_10901_10942_10907_11013_10962_10971_10968_10974_11032_11123_13007_12201_13203_16109_16201_17001_9999","graphEnv":"wise","nativeSdk":false,"sf":0,"isNaView":0,"isHalfWap":0}
     * tplData : {"defaultHide":false,"fortuneScore":{"total":88,"love":"86","work":"92","money":"88"},"text":{"desc":"今天工作起来觉得比较愉快，效率增加了很多。爱情方面需要润滑，不要让情感在平淡中疏远。财运方面今天不是太稳定，还是不要急于做决定，先进行合理分析才能做出正确判断。","do":"自拍","donot":"沉思"},"cardCollapseTop":true,"cardCollapseBottom":true}
     */

    private ExtDataBean extData;
    private CommonDataBean commonData;
    private TplDataBean tplData;

    @NoArgsConstructor
    @Data
    public static class ExtDataBean {
        /**
         * template_name : atom/fortuneToday
         * template_id : G694
         */

        private String template_name;
        private String template_id;
    }

    @NoArgsConstructor
    @Data
    public static class CommonDataBean {
        /**
         * feRoot : https://mms-static.cdn.bcebos.com/graph/graphfe/static
         * os : ios
         * sidList : 10007_10802_10915_10913_11006_10921_10905_10015_10901_10942_10907_11013_10962_10971_10968_10974_11032_11123_13007_12201_13203_16109_16201_17001_9999
         * graphEnv : wise
         * nativeSdk : false
         * sf : 0
         * isNaView : 0
         * isHalfWap : 0
         */

        private String feRoot;
        private String os;
        private String sidList;
        private String graphEnv;
        private boolean nativeSdk;
        private int sf;
        private int isNaView;
        private int isHalfWap;
    }

    @NoArgsConstructor
    @Data
    public static class TplDataBean {
        /**
         * defaultHide : false
         * fortuneScore : {"total":88,"love":"86","work":"92","money":"88"}
         * text : {"desc":"今天工作起来觉得比较愉快，效率增加了很多。爱情方面需要润滑，不要让情感在平淡中疏远。财运方面今天不是太稳定，还是不要急于做决定，先进行合理分析才能做出正确判断。","do":"自拍","donot":"沉思"}
         * cardCollapseTop : true
         * cardCollapseBottom : true
         */

        private boolean defaultHide;
        private FortuneScoreBean fortuneScore;
        private TextBean text;
        private boolean cardCollapseTop;
        private boolean cardCollapseBottom;

        @NoArgsConstructor
        @Data
        public static class FortuneScoreBean {
            /**
             * total : 88
             * love : 86
             * work : 92
             * money : 88
             */

            private int total;
            private String love;
            private String work;
            private String money;
        }

        @NoArgsConstructor
        @Data
        public static class TextBean {
            /**
             * desc : 今天工作起来觉得比较愉快，效率增加了很多。爱情方面需要润滑，不要让情感在平淡中疏远。财运方面今天不是太稳定，还是不要急于做决定，先进行合理分析才能做出正确判断。
             * do : 自拍
             * donot : 沉思
             */

            private String desc;
            private String doX;
            private String donot;
        }
    }
}
