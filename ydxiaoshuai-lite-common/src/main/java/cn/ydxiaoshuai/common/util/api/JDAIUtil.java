package cn.ydxiaoshuai.common.util.api;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpUtil;
import cn.ydxiaoshuai.common.api.vo.api.GarbageResponseBean;
import cn.ydxiaoshuai.common.api.vo.api.ImgColorResponseBean;
import cn.ydxiaoshuai.common.api.vo.api.RecommendResponseBean;
import cn.ydxiaoshuai.common.constant.JDAIConts;
import com.alibaba.fastjson.JSON;

import java.util.Comparator;
import java.util.List;

/**
 * @author 小帅丶
 * @className GarbageUtil
 * @Description 京东AI工具类
 * @Date 2020/4/26-16:39
 **/
public class JDAIUtil {

    /**
     * @Author 小帅丶
     * @Description 搭配生成
     * @Date  2020/10/26
     * @param imgBase64 图片的BASE64
     * @return java.lang.String
     **/
    public static RecommendResponseBean getRecommendBean(String imgBase64) throws Exception{
        String result = getRecommend(imgBase64);
        RecommendResponseBean recommendResponseBean = JSON.parseObject(result, RecommendResponseBean.class);
        return recommendResponseBean;
    }
    /**
     * @Author 小帅丶
     * @Description 颜色识别工具类
     * @Date  2020/10/26
     * @param imgBase64 图片的BASE64
     * @return java.lang.String
     **/
    public static ImgColorResponseBean getColorBean(String imgBase64) throws Exception{
        String result = getColor(imgBase64);
        ImgColorResponseBean imgColorResponseBean = JSON.parseObject(result, ImgColorResponseBean.class);
        return imgColorResponseBean;
    }
    /**
     * @Author 小帅丶
     * @Description 搭配生成
     * @Date  2020/10/26
     * @param imgBase64 图片的BASE64
     * @return java.lang.String
     **/
    public static String getRecommend (String imgBase64) throws Exception{
        //请求的时间戳，精确到毫秒，timestamp有效期5分钟
        String timestamp = String.valueOf(System.currentTimeMillis());
        //签名，根据规则MD5(sectetkey+timestamp)
        String sign = SecureUtil.md5(JDAIConts.SECRETKEY+timestamp);
        //URL参数
        String url_params = JDAIConts.RECOMMEND_OUTFITS_URL+"?appkey="+ JDAIConts.APPKEY+"&timestamp="+timestamp+"&sign="+sign;
        //BODY参数
        String body_params = "image="+ imgBase64 +"&outfit_num=10";
        //返回的结果
        String result = HttpRequest.post(url_params).header(Header.CONTENT_TYPE,"text/plain").body(body_params).execute().body();
        return result;
    }
    /**
     * @Author 小帅丶
     * @Description 颜色识别工具类
     * @Date  2020/10/26
     * @param imgBase64 图片的BASE64
     * @return java.lang.String
     **/
    public static String getColor(String imgBase64) throws Exception{
        //请求的时间戳，精确到毫秒，timestamp有效期5分钟
        String timestamp = String.valueOf(System.currentTimeMillis());
        //签名，根据规则MD5(sectetkey+timestamp)
        String sign = SecureUtil.md5(JDAIConts.SECRETKEY+timestamp);
        //URL参数
        String url_params = JDAIConts.EXTRACT_IMG_COLORS_URL+"?appkey="+ JDAIConts.APPKEY+"&timestamp="+timestamp+"&sign="+sign;
        System.out.println(url_params);
        //BODY参数
        String body_params = "image="+imgBase64+"&color_count=10";
        //返回的结果
        String result = HttpRequest.post(url_params).header(Header.CONTENT_TYPE,"text/plain").body(body_params).execute().body();
        return result;
    }
    /**
     * @Author 小帅丶
     * @Description 垃圾分类查询工具类
     * @Date  2020/4/26 16:45
     * @param cityId 城市ID
     *               支持城市：310000(上海市)、330200(宁波市)、610100(西安市)、440300(深圳市)、北京市(110000)
     * @param imgBase64 图片的BASE64
     * @return java.lang.String
     **/
    public static String getGarbage(String cityId,String imgBase64){
        //请求的时间戳，精确到毫秒，timestamp有效期5分钟
        String timestamp = String.valueOf(System.currentTimeMillis());
        //签名，根据规则MD5(sectetkey+timestamp)
        String sign = SecureUtil.md5(JDAIConts.SECRETKEY+timestamp);
        //URL参数
        String url_params = JDAIConts.GARBAGEIMAGESEARCH_URL+"?appkey="+ JDAIConts.APPKEY+"&timestamp="+timestamp+"&sign="+sign;
        //BODY参数
        String body_params = "{\"imgBase64\":\""+imgBase64+"\",\"cityId\":\""+cityId+"\"}";
        //返回的结果
        String result = HttpUtil.post(url_params, body_params);
        return result;
    }
    /**
     * @Author 小帅丶
     * @Description 垃圾分类查询工具类
     * @Date  2020/4/26 16:45
     * @param cityId 城市ID
     *               支持城市：310000(上海市)、330200(宁波市)、610100(西安市)、440300(深圳市)、北京市(110000)
     * @param imgBase64 图片的BASE64
     * @return GarbageResponseBean
     **/
    public static GarbageResponseBean getGarbageBean(String cityId, String imgBase64){
        String result = getGarbage(cityId, imgBase64);
        GarbageResponseBean garbageResponseBean = JSON.parseObject(result, GarbageResponseBean.class);
        if(garbageResponseBean.getResult().getStatus()==0){
            List<GarbageResponseBean.ResultBean.GarbageInfoBean> garbage_info = garbageResponseBean.getResult().getGarbage_info();
            garbage_info.sort(Comparator.comparing(GarbageResponseBean.ResultBean.GarbageInfoBean::getConfidence).reversed());
            return garbageResponseBean;
        }else{
            return garbageResponseBean;
        }
    }
}
