package cn.ydxiaoshuai.common.constant;

/**
 * @author 小帅丶
 * @className APICodeConts
 * @Description 常量类
 * @Date 2020/3/25-11:59
 **/
public class ApiCodeConts {
    //加积分
    public static final String INTEGRAL_STATUS_ADD = "add";
    //减积分
    public static final String INTEGRAL_STATUS_REDUCE = "reduce";
    //积分商品兑换
    public static final String INTEGRAL_EXCHANGE_PRODUCT = "积分兑换";
    //默认IP
    public static final String DEFAULT_IP = "999";
    //请求成功
    public static final String MESSAGE_OK = "OK";
    //请求失败
    public static final String MESSAGE_FAIL = "FAIL";
    //请求一次
    public static final String MESSAGE_ERROR = "ERROR";
    //系统错误
    public static final String MESSAGE_ERROR_TEXT = "系统错误，请稍后再试";
    //无内容
    public static final Integer LIST_NULL = 20000;
    //参数缺失
    public static final String LIST_NULL_TEXT = "用户信息不存在";
    //参数为空
    public static final Integer PARAM_NULL = 20001;
    //网点不存在
    public static final Integer DOT_CODE_NULL = 30000;
    //参数缺失
    public static final Integer PARAM_DEFECT = 40002;
    //参数缺失
    public static final String PARAM_DEFECT_TEXT = "参数缺失，请检查";
    //用户渠道类型错误
    public static final Integer PARAM_CHANNEL_ERROR = 40007;
    //用户渠道类型错误
    public static final String PARAM_CHANNEL_ERROR_TEXT = "用户渠道类型错误";
    //默认创建人
    public static final String API_LOG_CREATE_BY = "system";
    //apilog日志
    public static final String API_LOG_LIST_KEY = "api_log";
}
