package cn.ydxiaoshuai.common.constant;

/**
 * @Description 手相分析接口地址
 * @author 小帅丶
 * @className PalmConts
 * @Date 2020/1/3-10:51
 **/
public class PalmConts {
    //上传图片接口地址-微算
    public static String UPLOAD  = "https://pubhand.qqwechat.com/h1/UploadHandImg";
    //上传图片接口地址
    public static String UPIMG_URL_POST = "https://aliyun.jienju.cn/api/tools/images";
    //获取ID接口地址
    public static String IMGURL_ID_POST = "https://hand-service.linghitai.com/hand/uploadImgUrl";
    //获取手部关键点位置信息接口地址
    public static String HAND_POLLING_GET = "https://hand-service.linghitai.com/hand/polling?id=ID";
    //获取订单号接口地址
    public static String HAND_ORDER_POST = "https://aliyun.jienju.cn/api/orders?channel=swxz&extend_info=EXTEND_INFO&product_id=shou_xiang_yan_jiu_yuan&special_info=SPECIAL_INFO";
    //获取手相分析数据
    public static String HAND_DATA_GET = "https://aliyun.jienju.cn/api/results/detain/ORDERID";

    //上传图片接口地址
    //public static String UPIMG_URL_POST = "https://zxcs.lingcy365.cn/api/tools/images";
    //获取ID接口地址
    //public static String IMGURL_ID_POST = "https://hand-service.linghitai.com/hand/uploadImgUrl";
    //获取手部关键点位置信息接口地址
    //public static String HAND_POLLING_GET = "https://hand-service.linghitai.com/hand/polling?id=ID";
    //获取订单号接口地址
    //public static String HAND_ORDER_POST = "https://zxcs.lingcy365.cn/api/orders?channel=swdywlcm000&extend_info=EXTEND_INFO&product_id=shou_xiang_yan_jiu_yuan&special_info=SPECIAL_INFO";
    //获取手相分析数据
    //public static String HAND_DATA_GET = "https://zxcs.lingcy365.cn/api/results/detain/ORDERID";
}
