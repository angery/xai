package cn.ydxiaoshuai.common.constant;

/**
 * @Description 灵机文化AI接口地址
 * @author 小帅丶
 * @className LinghitAIConts
 * @Date 2020/1/10-16:20
 **/
public class LinghitAIConts {
    /**人脸变老接口地址*/
    public static String GROWOLD_URL_POST = "https://apis.linghitai.com/image/face/faceAgeing";
    /**性别反转接口地址*/
    public static String GENDERCONVERTION_URL_POST = "https://apis.linghitai.com/image/face/genderConvertion";
    /**我的动物脸接口地址*/
    public static String FACEANIMALSIMILARITY_URL_POST = "https://apis.linghitai.com/image/face/faceAnimalSimilarity";
}
