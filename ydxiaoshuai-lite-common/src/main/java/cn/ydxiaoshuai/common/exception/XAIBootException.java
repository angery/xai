package cn.ydxiaoshuai.common.exception;

public class XAIBootException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public XAIBootException(String message){
		super(message);
	}
	
	public XAIBootException(Throwable cause)
	{
		super(cause);
	}
	
	public XAIBootException(String message, Throwable cause)
	{
		super(message,cause);
	}
}
