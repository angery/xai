package cn.ydxiaoshuai.modules.service.impl;

import cn.ydxiaoshuai.modules.entity.LiteApiLog;
import cn.ydxiaoshuai.modules.mapper.LiteApiLogMapper;
import cn.ydxiaoshuai.modules.service.ILiteApiLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @Description: API日志记录表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
@Service
public class LiteApiLogServiceImpl extends ServiceImpl<LiteApiLogMapper, LiteApiLog> implements ILiteApiLogService {
    @Resource
    private LiteApiLogMapper liteApiLogMapper;
    @Override
    public List<Map<String, Object>> getAllCount() {
        List<Map<String, Object>> mapList = liteApiLogMapper.getAllCount();
        return mapList;
    }

    @Override
    public List<Map<String, Object>> getUserRank() {
        List<Map<String, Object>> mapList = liteApiLogMapper.getUserRank();
        return mapList;
    }

    @Override
    public List<Map<String, Object>> getAllCountToDay() {
        List<Map<String, Object>> mapList = liteApiLogMapper.getAllCountToDay();
        return mapList;
    }

    @Override
    public List<Map<String, Object>> getUserRankToDay() {
        List<Map<String, Object>> mapList = liteApiLogMapper.getUserRankToDay();
        return mapList;
    }
}
