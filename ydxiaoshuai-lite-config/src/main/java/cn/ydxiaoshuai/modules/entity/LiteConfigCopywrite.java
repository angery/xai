package cn.ydxiaoshuai.modules.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 刷新文案
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Data
@TableName("lite_config_copywrite")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="lite_config_copywrite对象", description="刷新文案")
public class LiteConfigCopywrite {
    
	/**主键ID*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键ID")
	private String id;
	/**上刷新内容*/
	@Excel(name = "上刷新内容", width = 15)
    @ApiModelProperty(value = "上刷新内容")
	private String aboveText;
	/**下刷新内容*/
	@Excel(name = "下刷新内容", width = 15)
    @ApiModelProperty(value = "下刷新内容")
	private String belowText;
	/**是否可用 1不可用 0可用*/
	@Excel(name = "是否可用 1不可用 0可用", width = 15)
    @ApiModelProperty(value = "是否可用 1不可用 0可用")
	private Integer isEnable;
	/**创建者*/
	@Excel(name = "创建者", width = 15)
    @ApiModelProperty(value = "创建者")
	private String createBy;
	/**创建时间*/
	@Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
	private Date createTime;
	/**更新者*/
	@Excel(name = "更新者", width = 15)
    @ApiModelProperty(value = "更新者")
	private String updateBy;
	/**更新时间*/
	@Excel(name = "更新时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
	private Date updateTime;
}
