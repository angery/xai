package cn.ydxiaoshuai.modules.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import cn.ydxiaoshuai.common.api.vo.Result;
import cn.ydxiaoshuai.common.system.query.QueryGenerator;
import cn.ydxiaoshuai.common.aspect.annotation.AutoLog;
import cn.ydxiaoshuai.common.util.oConvertUtils;
import cn.ydxiaoshuai.modules.entity.LiteConfigRollnews;
import cn.ydxiaoshuai.modules.service.ILiteConfigRollnewsService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import cn.ydxiaoshuai.common.system.base.controller.JeecgController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 首页滚动公告表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
@Slf4j
@Api(tags="首页滚动公告表")
@RestController
@RequestMapping("/config/liteConfigRollnews")
public class LiteConfigRollnewsController extends JeecgController<LiteConfigRollnews, ILiteConfigRollnewsService> {
	@Autowired
	private ILiteConfigRollnewsService liteConfigRollnewsService;
	
	/**
	 * 分页列表查询
	 *
	 * @param liteConfigRollnews
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "首页滚动公告表-分页列表查询")
	@ApiOperation(value="首页滚动公告表-分页列表查询", notes="首页滚动公告表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(LiteConfigRollnews liteConfigRollnews,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<LiteConfigRollnews> queryWrapper = QueryGenerator.initQueryWrapper(liteConfigRollnews, req.getParameterMap());
		Page<LiteConfigRollnews> page = new Page<LiteConfigRollnews>(pageNo, pageSize);
		IPage<LiteConfigRollnews> pageList = liteConfigRollnewsService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param liteConfigRollnews
	 * @return
	 */
	@AutoLog(value = "首页滚动公告表-添加")
	@ApiOperation(value="首页滚动公告表-添加", notes="首页滚动公告表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody LiteConfigRollnews liteConfigRollnews) {
		liteConfigRollnewsService.save(liteConfigRollnews);
		return Result.ok("添加成功！");
	}
	
	/**
	 * 编辑
	 *
	 * @param liteConfigRollnews
	 * @return
	 */
	@AutoLog(value = "首页滚动公告表-编辑")
	@ApiOperation(value="首页滚动公告表-编辑", notes="首页滚动公告表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody LiteConfigRollnews liteConfigRollnews) {
		liteConfigRollnewsService.updateById(liteConfigRollnews);
		return Result.ok("编辑成功!");
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "首页滚动公告表-通过id删除")
	@ApiOperation(value="首页滚动公告表-通过id删除", notes="首页滚动公告表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		liteConfigRollnewsService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "首页滚动公告表-批量删除")
	@ApiOperation(value="首页滚动公告表-批量删除", notes="首页滚动公告表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.liteConfigRollnewsService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "首页滚动公告表-通过id查询")
	@ApiOperation(value="首页滚动公告表-通过id查询", notes="首页滚动公告表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		LiteConfigRollnews liteConfigRollnews = liteConfigRollnewsService.getById(id);
		return Result.ok(liteConfigRollnews);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param liteConfigRollnews
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, LiteConfigRollnews liteConfigRollnews) {
      return super.exportXls(request, liteConfigRollnews, LiteConfigRollnews.class, "首页滚动公告表");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, LiteConfigRollnews.class);
  }

}
