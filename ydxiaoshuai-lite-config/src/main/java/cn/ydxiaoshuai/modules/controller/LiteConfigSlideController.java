package cn.ydxiaoshuai.modules.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import cn.ydxiaoshuai.common.api.vo.Result;
import cn.ydxiaoshuai.common.system.query.QueryGenerator;
import cn.ydxiaoshuai.common.aspect.annotation.AutoLog;
import cn.ydxiaoshuai.common.util.oConvertUtils;
import cn.ydxiaoshuai.modules.entity.LiteConfigSlide;
import cn.ydxiaoshuai.modules.service.ILiteConfigSlideService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import cn.ydxiaoshuai.common.system.base.controller.JeecgController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

 /**
 * @Description: 首页轮播图配置表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
@Slf4j
@Api(tags="首页轮播图配置表")
@RestController
@RequestMapping("/config/liteConfigSlide")
public class LiteConfigSlideController extends JeecgController<LiteConfigSlide, ILiteConfigSlideService> {
	@Value(value = "${xai.domain}")
	private String domain;
	@Autowired
	private ILiteConfigSlideService liteConfigSlideService;
	
	/**
	 * 分页列表查询
	 *
	 * @param liteConfigSlide
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "首页轮播图配置表-分页列表查询")
	@ApiOperation(value="首页轮播图配置表-分页列表查询", notes="首页轮播图配置表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(LiteConfigSlide liteConfigSlide,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<LiteConfigSlide> queryWrapper = QueryGenerator.initQueryWrapper(liteConfigSlide, req.getParameterMap());
		Page<LiteConfigSlide> page = new Page<LiteConfigSlide>(pageNo, pageSize);
		IPage<LiteConfigSlide> pageList = liteConfigSlideService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 * 添加
	 *
	 * @param liteConfigSlide
	 * @return
	 */
	@AutoLog(value = "首页轮播图配置表-添加")
	@ApiOperation(value="首页轮播图配置表-添加", notes="首页轮播图配置表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody LiteConfigSlide liteConfigSlide) {
		Result<LiteConfigSlide> result = new Result<LiteConfigSlide>();
		try {
			liteConfigSlide.setImgUrl(domain + liteConfigSlide.getImgPath());
			liteConfigSlideService.save(liteConfigSlide);
			result.success("新增成功!");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result.error500("操作失败");
		}
		return result;
	}
	
	/**
	 * 编辑
	 *
	 * @param liteConfigSlide
	 * @return
	 */
	@AutoLog(value = "首页轮播图配置表-编辑")
	@ApiOperation(value="首页轮播图配置表-编辑", notes="首页轮播图配置表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody LiteConfigSlide liteConfigSlide) {
		Result<LiteConfigSlide> result = new Result<LiteConfigSlide>();
		LiteConfigSlide ismStoreConfigNavEntity = liteConfigSlideService.getById(liteConfigSlide.getId());
		if (ismStoreConfigNavEntity == null) {
			result.error500("未找到对应实体");
		} else {
			if(!ismStoreConfigNavEntity.getImgPath().equals(liteConfigSlide.getImgPath())){
				liteConfigSlide.setImgUrl(domain + liteConfigSlide.getImgPath());
			}
			boolean ok = liteConfigSlideService.updateById(liteConfigSlide);
			if (ok) {
				result.success("修改成功!");
			}
		}
		return result;
	}
	
	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "首页轮播图配置表-通过id删除")
	@ApiOperation(value="首页轮播图配置表-通过id删除", notes="首页轮播图配置表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		liteConfigSlideService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "首页轮播图配置表-批量删除")
	@ApiOperation(value="首页轮播图配置表-批量删除", notes="首页轮播图配置表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.liteConfigSlideService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "首页轮播图配置表-通过id查询")
	@ApiOperation(value="首页轮播图配置表-通过id查询", notes="首页轮播图配置表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		LiteConfigSlide liteConfigSlide = liteConfigSlideService.getById(id);
		return Result.ok(liteConfigSlide);
	}

  /**
   * 导出excel
   *
   * @param request
   * @param liteConfigSlide
   */
  @RequestMapping(value = "/exportXls")
  public ModelAndView exportXls(HttpServletRequest request, LiteConfigSlide liteConfigSlide) {
      return super.exportXls(request, liteConfigSlide, LiteConfigSlide.class, "首页轮播图配置表");
  }

  /**
   * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
  @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
  public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      return super.importExcel(request, response, LiteConfigSlide.class);
  }

}
