package cn.ydxiaoshuai.modules.service.impl;

import cn.ydxiaoshuai.modules.entity.LiteConfigCopywrite;
import cn.ydxiaoshuai.modules.mapper.LiteConfigCopywriteMapper;
import cn.ydxiaoshuai.modules.service.ILiteConfigCopywriteService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 刷新文案
 * @Author: 小帅丶
 * @Date:   2020-05-14
 * @Version: V1.0
 */
@Service
public class LiteConfigCopywriteServiceImpl extends ServiceImpl<LiteConfigCopywriteMapper, LiteConfigCopywrite> implements ILiteConfigCopywriteService {

}
