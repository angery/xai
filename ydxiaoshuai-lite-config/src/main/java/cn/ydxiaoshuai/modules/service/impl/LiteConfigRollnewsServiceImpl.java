package cn.ydxiaoshuai.modules.service.impl;

import cn.ydxiaoshuai.modules.entity.LiteConfigRollnews;
import cn.ydxiaoshuai.modules.mapper.LiteConfigRollnewsMapper;
import cn.ydxiaoshuai.modules.service.ILiteConfigRollnewsService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 首页滚动公告表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
@Service
public class LiteConfigRollnewsServiceImpl extends ServiceImpl<LiteConfigRollnewsMapper, LiteConfigRollnews> implements ILiteConfigRollnewsService {

}
