package cn.ydxiaoshuai.modules.mapper;

import cn.ydxiaoshuai.modules.entity.LiteConfigNav;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 首页菜单配置表
 * @Author: 小帅丶
 * @Date:   2020-04-30
 * @Version: V1.0
 */
public interface LiteConfigNavMapper extends BaseMapper<LiteConfigNav> {

}
