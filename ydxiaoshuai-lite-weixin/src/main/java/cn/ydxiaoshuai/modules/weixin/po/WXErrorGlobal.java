package cn.ydxiaoshuai.modules.weixin.po;

import lombok.Data;

/**
 * @Description 全局错误对象
 * @author 小帅丶
 * @className WXErrorGlobal
 * @Date 2019/11/28-10:45
 **/
@Data
public class WXErrorGlobal {
    //错误码
    private Integer errcode;
    //错误描述
    private String errmsg;
}
