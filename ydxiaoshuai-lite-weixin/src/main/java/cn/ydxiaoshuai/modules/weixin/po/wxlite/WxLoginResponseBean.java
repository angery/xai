package cn.ydxiaoshuai.modules.weixin.po.wxlite;

import cn.ydxiaoshuai.common.api.vo.BaseResponseBean;
import cn.ydxiaoshuai.common.constant.CommonConstant;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description wx.login
 * @author 小帅丶
 * @className WxLoginResponseBean
 * @Date 2019/8/5-16:01
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WxLoginResponseBean extends BaseResponseBean {
    private static final long serialVersionUID = 1L;
    private Data data;
    @lombok.Data
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Data{
        private String openid;
        private String session_key;
        private Integer days=0;
    }
    public WxLoginResponseBean success(String message,Data data) {
        this.message = message;
        this.code = CommonConstant.SC_OK_200;
        this.data = data;
        return this;
    }
    public WxLoginResponseBean success(String message) {
        this.message = message;
        this.code = CommonConstant.SC_OK_200;
        return this;
    }

    public WxLoginResponseBean ok(Data data) {
        this.code = CommonConstant.SC_OK_200;
        this.data = data;
        return this;
    }
    public WxLoginResponseBean error(String message) {
        this.message = message;
        this.code = CommonConstant.SC_INTERNAL_SERVER_ERROR_500;
        return this;
    }
    public WxLoginResponseBean fail(String message,Integer code) {
        this.message = message;
        this.code = code;
        return this;
    }
}
